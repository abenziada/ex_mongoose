const express = require('express')
const Animal = require('../animal.js')
const router = express.Router()

router.post('/animal',(req,res,next)=>{
    console.log('farmer adds animal')
    const animal = new Animal({
        name:'Alex',
        kind:'Dog',
        age:5
    })
    
    animal.save().then(result=>{
        console.log(result)
        res.send(animal)
    })
})

router.get('/animal',(req,res,err)=>{
    Animal.find()
        .then(animals=>{
            res.send(animals)
            console.log('All animals sent.')
        })
})

router.get('/', (req,res,err) => {
    res.send("<h1>Salut ma Rome adorée ^^</h1>")
})

module.exports = router
