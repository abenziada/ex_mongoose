const mongoose = require("mongoose")

const Schema = mongoose.Schema

const animalSchema = new Schema({
    name:String,
    kind:{
        type:String,
        required:true
    },
    age:Number
});

module.exports = mongoose.model('Animal',animalSchema)