const express = require("express")
const bodyParser = require("body-parser")
const mongoose = require("mongoose")

const Animal = require('./animal.js')
const farmerRoutes = require('./routes/farmer.js')

const app = express()
app.use(bodyParser.urlencoded({extended:false}))

//Plan
//Create Express middleware to handle GET POST DEL operations

app.use(farmerRoutes)

const dbPath = "mongodb://mongo:27017/ex_mongoose_db"
mongoose.connect(dbPath,{useNewUrlParser:true, useUnifiedTopology: true })
        .then(result=>{
            console.log("Connected to database... ya yaw !")
        })
        .catch(err=>{
            console.log("Error when connecting to database. "+err.message)
        })

app.listen(3000,()=>{
    console.log("Server listening to port 3000... MSG FROM DOCKER")
})